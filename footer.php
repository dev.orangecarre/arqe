</main>
    
    <footer class="footer">
        <aside class="footer__confiance">
            <h2 class="footer__confiance-titre">Ils nous font confiance</h2>
            <div class="footer__confiance-entreprises">
                <article class="footer__confiance-entreprise"><img src="<?php echo get_template_directory_uri() . '/dist/images/logo-arqe.svg'; ?>" alt="" class="footer__confiance-logo"></article>
                <article class="footer__confiance-entreprise"><img src="<?php echo get_template_directory_uri() . '/dist/images/logo-arqe.svg'; ?>" alt="" class="footer__confiance-logo"></article>
                <article class="footer__confiance-entreprise"><img src="<?php echo get_template_directory_uri() . '/dist/images/logo-arqe.svg'; ?>" alt="" class="footer__confiance-logo"></article>
                <article class="footer__confiance-entreprise"><img src="<?php echo get_template_directory_uri() . '/dist/images/logo-arqe.svg'; ?>" alt="" class="footer__confiance-logo"></article>
            </div>
        </aside>
        <section class="footer__colonnes">
            <div class="footer__colonne footer__colonne--gauche">
                <img src="<?php echo get_template_directory_uri() . '/dist/images/logo-arqe.svg'; ?>" alt="Logo ARQE" class="footer__colonne-logo">
                <h3 class="footer__colonne-titre">Association RQE</h3>
                <strong class="footer__colonne-label">Nous trouver :</strong>
                <p class="footer__colonne-contenu">79, Rue Henri Gautier 93000, Bobigny</p>
                <strong class="footer__colonne-label">Nous écrire</strong>
                <p class="footer__colonne-contenu">info@rqe-france.org</p>
                <div class="footer__colonne-socials">
                    <a href="" class="footer__colonne-social">
                        <img src="<?php echo get_template_directory_uri() . '/dist/images/facebook.svg'; ?>" alt="logo facebook" class="footer__colonne-social-img">
                    </a>
                    <a href="" class="footer__colonne-social">
                        <img src="<?php echo get_template_directory_uri() . '/dist/images/twitter.svg'; ?>" alt="logo twitter" class="footer__colonne-social-img">
                    </a>
                    <a href="" class="footer__colonne-social">
                        <img src="<?php echo get_template_directory_uri() . '/dist/images/instagram.svg'; ?>" alt="logo instagram" class="footer__colonne-social-img">
                    </a>
                    <a href="" class="footer__colonne-social">
                        <img src="<?php echo get_template_directory_uri() . '/dist/images/linkedin.svg'; ?>" alt="logo linkedin" class="footer__colonne-social-img">
                    </a>
                </div>
            </div>
            <div class="footer__colonne">
                <div class="footer__green-logos">
                    <div class="footer__menu">
                        <a href="" class="footer__lien">Plan du site</a>
                        <?php
                        wp_nav_menu(
                            array(
                                'menu' => 'footer_menu',
                                'container' => 'div',
                                'container_class' => 'footer-menu',
                                'items_wrap' => '<ul class="footer-menu__elements">%3$s</ul>'
                            )
                        );
                        ?>
                    </div>
                    <div class="footer__green-logo-conteneur">
                        <img src="<?php echo get_template_directory_uri() . '/dist/images/logo-changer-derrre.png'; ?>" alt="" class="footer__green-logo">
                        <img src="<?php echo get_template_directory_uri() . '/dist/images/logo-chantier-zero-carbone.png'; ?>" alt="" class="footer__green-logo">
                        <img src="<?php echo get_template_directory_uri() . '/dist/images/logo-eco-site.svg'; ?>" alt="" class="footer__green-logo">
                    </div>               
                </div>
            </div>
        </section>
        <section class="footer__liens">
          <a href="<?php echo get_home_url();; ?>/privacy-policy/" class="footer__liens-mentions">Politique de confidentialité </a>
          <span class="footer__liens-separator"> - </span>
          <a href="https://orangecarre.fr" class="footer__liens-orange"> © L'Orange Carré</a>
        </section>
    </footer>

<?php wp_footer(); ?>


<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
};

var slideIndexSecond = 1;
showDivsSecond(slideIndexSecond);

function plusDivsSecond(m) {
  showDivsSecond(slideIndexSecond += m);
}

function showDivsSecond(m) {
  var f;
  var g = document.getElementsByClassName("mySlidesSecond");
  if (m > g.length) {slideIndexSecond = 1}
  if (m < 1) {slideIndexSecond = g.length}
  for (f = 0; f < g.length; f++) {
    g[f].style.display = "none";  
  }
  g[slideIndexSecond-1].style.display = "block";  
}	

</script>

<script>
    //sub-menu
    const hasChildren = document.querySelectorAll(".menu-item-has-children > a:first-of-type");
  const allContent = document.querySelectorAll(".sub-menu");

  hasChildren.forEach((subMenu) => {
    subMenu.addEventListener("click", function (event) {
      const target = event.target;
      const subMenuList = event.target.nextElementSibling;
      event.preventDefault();
      if (!subMenuList.classList.contains("active")) {
        //allContent.forEach((item) => item.classList.remove("active")); //only allow one child open at a time (doesn't work with nested)
        //hasChildren.forEach((elem) => elem.classList.remove("expanded"));
      }
      target.classList.toggle("expanded");
      subMenuList.classList.toggle("active");
    });
  });
  
</script>

</body>

</html>
