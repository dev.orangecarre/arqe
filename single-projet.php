<?php get_header(); ?>

<?php if (have_posts()) {
    while (have_posts()) {
        the_post(); ?>
        <header class="projet-header">
            <div class="projet-header__img-conteneur"><img src="<?php the_post_thumbnail_url(); ?>" alt="" class="projet-header__img"></div>
            <article class="container projet-header__contenu">
                <h1 class="projet-header__titre"><?php the_title(); ?></h1>
                <h2 class="projet-header__sous-titre">
                    <strong>
                        <?php the_field('sous_titre'); ?>
                    </strong>
                </h2>
                <p class="projet-header__texte">
                    <?php the_field('description_article'); ?>
                </p>
            </article>
        </header>

        <article class="container-content">

            <?php the_content(); ?>

        </article>

<?php
    }
} ?>

<?php get_footer(); ?>