<?php

include_once locate_template('/config/blog/index.php');

include_once locate_template('/config/clean/index.php');

include_once locate_template('/config/customizer/index.php');

include_once locate_template('/config/enqueue.php');

include_once locate_template('/config/editor/index.php');

include_once locate_template('/config/menus.php');

include_once locate_template('/config/theme_support.php');

function custom_excerpt_length($length)
{
    return 15;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

define('FS_METHOD', 'direct');
