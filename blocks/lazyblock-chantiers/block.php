<section class="section__chantiers">
    <header class="chantiers__header">
        <h2 class="chantiers__title"><?php echo $attributes['titre-section-chantier']; ?></h2>
    </header>
    <div class="chantiers__content">
    <?php
        $args = array(
            'post_type' => 'chantier',
            'posts_per_page' => 10
        );
        $the_query = new WP_Query($args); ?>

        <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <article class="chantier__article">
            <img class="chantier__img" alt="" src="<?php the_post_thumbnail(); ?>"  >
            <h3 class="chantier__title"><?php the_title(); ?></h3>
            <a class="chantier__btn bouton" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >+ d'infos</a>
        </article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</section>
