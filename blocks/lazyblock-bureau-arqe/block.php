<section class="section section__bureau container">
    <article class="bureau__article bureau__article--dirigent">
        <header class="bureau__articleHeader">
            <h3 class="bureau__subTitle">direction</h3>
        </header>
        <main class="bureau__articleContent">
            <div class="bureau__poste">
                <h4 class="bureau__postName"> Président/Trésorier
                </h4>
                <div class="bureau__member">
                    <p class="bureau__text">
                        <?php the_field('nom_president_tresorier'); ?>
                    </p>
                    <a class="bureau__link" href="<?php the_field('site_president_tresorier'); ?>" target="_blank" rel="noopener noreferrer">
                        <?php the_field('texte_lien_site_president_tresorier'); ?>
                    </a>
                </div>
            </div>

            <div class="bureau__poste">
                <h4 class="bureau__postName">Vice président</h4>
                <div class="bureau__member">
                    <p class="bureau__text">
                        <?php the_field('nom_vice_president'); ?>
                    </p>
                    <a class="bureau__link" href="<?php the_field('site_vice_president'); ?>" target="_blank" rel="noopener noreferrer">
                        <?php the_field('texte_lien_site_vice_president'); ?>
                    </a>
                </div>
            </div>

            <div class="bureau__poste">
                <h4 class="bureau__postName">Secrétaire</h4>
                <div class="bureau__member">
                    <p class="bureau__text">
                        <?php the_field('nom_secretaire'); ?>
                    </p>
                    <a class="bureau__link" href="<?php the_field('site_secretaire'); ?>" target="_blank" rel="noopener noreferrer">
                        <?php the_field('texte_lien_site_secretaire'); ?>
                    </a>
                </div>
            </div>
        </main>
    </article>

    <article class="bureau__article bureau__article--commission">
        <header class="bureau__articleHeader">
            <h3 class="bureau__subTitle">commission d'agréments</h3>
        </header>

        <main class="bureau__articleContent">
            <div class="bureau__poste">
                <h4 class="bureau__postName bureau__postName--isLonger">Collège INDUSTRIE
                    ET COMMERCE</h4>

                <div class="bureau__member">
                    <p class="bureau__text"><?php the_field('nom_premier_membre_industrie'); ?></p>
                    <a class="bureau__link" href="<?php the_field('site_premier_membre_industrie'); ?>" target="_blank" rel="noopener noreferrer"><?php the_field('texte_lien_site_premier_membre_industrie'); ?></a>
                </div>

                <div class="bureau__member">
                    <p class="bureau__text"><?php the_field('nom_second_membre_industrie'); ?></p>
                    <a class="bureau__link" href="<?php the_field('site_second_membre_industrie'); ?>" target="_blank" rel="noopener noreferrer"><?php the_field('texte_lien_site_second_membre_industrie'); ?></a>
                </div>
            </div>

            <div class="bureau__poste">

                <h4 class="bureau__postName bureau__postName--isLonger">Collège MAITRISE
                    ET SERVICE</h4>
                <div class="bureau__member">
                    <p class="bureau__text"><?php the_field('nom_premier_membre_maitrise'); ?></p>
                    <a class="bureau__link" href="<?php the_field('site_premier_membre_maitrise'); ?>" target="_blank" rel="noopener noreferrer">
                        <?php the_field('texte_lien_site_premier_membre_maitrise'); ?>
                    </a>
                </div>
                <div class="bureau__member">
                    <p class="bureau__text"><?php the_field('nom_second_membre_maitrise'); ?></p>
                    <a class="bureau__link" href="<?php the_field('site_second_membre_maitrise'); ?>" target="_blank" rel="noopener noreferrer">
                        <?php the_field('texte_lien_site_second_membre_maitrise'); ?>
                    </a>
                </div>
            </div>

            <div class="bureau__poste">
                <h4 class="bureau__postName bureau__postName--isLonger">Collège BATIMENT –
                    PEINTURE – ISOLATION
                    TOUS CORPS D’ÉTAT</h4>
                <div class="bureau__member">
                    <p class="bureau__text">
                        <?php the_field('nom_premier_membre_bâtiment'); ?>
                    </p>
                    <a class="bureau__link" href="<?php the_field('site_premier_membre_bâtiment'); ?>">
                        <?php the_field('texte_lien_site_premier_membre_bâtiment'); ?>
                    </a>
                </div>
                <div class="bureau__member">
                    <p class="bureau__text">
                        <?php the_field('nom_second_membre_bâtiment'); ?>
                    </p>
                    <a class="bureau__link" href="<?php the_field('site_second_membre_bâtiment'); ?>" target="_blank" rel="noopener noreferrer">
                        <?php the_field('texte_lien_site_second_membre_bâtiment'); ?>
                    </a>
                </div>
            </div>
        </main>
    </article>

    <article class="bureau__article bureau__article--demarche">
        <img class="bureau__image" src="<?php echo esc_url($attributes['illustration-page-bureau']['url']); ?>" alt="<?php echo $attributes['texte-alternatif-illustration']; ?>">
        <main>
            <p class="bureau__text bureau__text--noBG"><?php echo $attributes['texte-demarche']; ?></p>
            <p class="bureau__text bureau__text--noBG"><?php echo $attributes['source-demarche']; ?></p>
        </main>
    </article>
</section>

<section class="section section__chiffres ">
    <header class="chiffres__header">
        <h2 class="chiffres__title"><?php echo $attributes['titre-section-chiffre']; ?>&nbsp;<time datetime="2020-01-01"><?php echo $attributes['date-chiffres']; ?></time></h2>
    </header>
    <article class="chiffres__article">
        <h3 class="chiffres__subTitle chiffres__subTitle--isLeft"><?php echo $attributes['sous-titre-batiment']; ?></h3>
        <main class="chiffres__content">
            <?php foreach ($attributes['serie-chiffres-une'] as $inner) : ?>
                <figure class="chiffres__figure">
                    <img class="chiffres__img" src="<?php echo esc_url($inner['logo-chiffre-premiere-serie']['url']); ?>" alt="<?php echo $inner['alt-logo-chiffre-premiere-serie']; ?>">
                    <figcaption class="chiffres__figcaption">
                        <p class="chiffres__text"><strong class="chiffres__text chiffres__text--isStrong"><?php echo $inner['valeur-chiffre-premiere-serie']; ?></strong></p>
                        <p class="chiffres__text">
                            <?php echo $inner['legende-chiffre-premiere-serie']; ?>
                        </p>
                    </figcaption>
                </figure>
            <?php endforeach; ?>
        </main>
    </article>


    <article class="chiffres__article">
        <h3 class="chiffres__subTitle chiffres__subTitle--isRight"><?php echo $attributes['sous-titre-chantier-zero-carbone']; ?></h3>
        <main class="chiffres__content">
            <?php foreach ($attributes['serie-chiffres-deux'] as $inner) : ?>
                <figure class="chiffres__figure">
                    <img class="chiffres__img" src="<?php echo esc_url($inner['logo-chiffre-seconde-serie']['url']); ?>" alt="<?php echo $inner['alt-logo-chiffre-seconde-serie']; ?>">
                    <figcaption class="chiffres__figcaption">
                        <p class="chiffres__text"><strong class="chiffres__text chiffres__text--isStrong"><?php echo $inner['valeur-chiffre-seconde-serie']; ?></strong></p>
                        <p class="chiffres__text">
                            <?php echo $inner['legende-chiffre-seconde-serie']; ?>
                        </p>
                    </figcaption>
                </figure>
            <?php endforeach; ?>
        </main>
    </article>
</section>