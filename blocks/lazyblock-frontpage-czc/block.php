<section class="home-czc">
    <h2 class="home-czc__titre"><?php echo $attributes['titre']; ?></h2>
        <div class="home-czc__acteurs">
            <article class="home-czc__acteur on-hover">
                <h3 class="home-czc__acteur-titre"><?php echo $attributes['titre-acteur-1']; ?></h3>
                <img src="<?php echo get_template_directory_uri() . '/dist/images/famille01.svg'; ?>" alt="" class="home-czc__acteur-img">
                <div class="home-czc__overlay">
                    <h4 class="home-czc__overlay-titre"><?php echo $attributes['titre-contenu-acteur-1']; ?></h4>
                    <p class="home-czc__overlay-description">
                        <?php echo $attributes['contenu-acteur-1']; ?>
                    </p>
                </div>
            </article>
            <article class="home-czc__acteur on-hover">
                <h3 class="home-czc__acteur-titre"><?php echo $attributes['titre-acteur-2']; ?></h3>
                <img src="<?php echo get_template_directory_uri() . '/dist/images/famille02.svg'; ?>" alt="" class="home-czc__acteur-img">
                <div class="home-czc__overlay">
                    <h4 class="home-czc__overlay-titre"><?php echo $attributes['titre-contenu-acteur-2']; ?></h4>
                    <p class="home-czc__overlay-description">
                        <?php echo $attributes['contenu-acteur-2']; ?>
                    </p>
                </div>
            </article>
            <article class="home-czc__acteur on-hover">
                <h3 class="home-czc__acteur-titre"><?php echo $attributes['titre-acteur-3']; ?></h3>
                <img src="<?php echo get_template_directory_uri() . '/dist/images/famille03.svg'; ?>" alt="" class="home-czc__acteur-img">
                <div class="home-czc__overlay">
                    <h4 class="home-czc__overlay-titre"><?php echo $attributes['titre-contenu-acteur-3']; ?></h4>
                    <p class="home-czc__overlay-description">
                    <?php echo $attributes['contenu-acteur-3']; ?>
                    </p>
                </div>
            </article>
        </div>
</section>