<section class="container section__actualites">
    <div class="actualites__content">
        <?php
        $args = array(
            'posts_per_page' => 10
        );
        $the_query = new WP_Query($args); ?>

        <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <article class="actualite__article">
                    <img class="actualite__img" alt="" src="<?php the_post_thumbnail(); ?>">
                    <div class="actualite__infos">
                        <h3 class="actualite__title"><?php the_title(); ?></h3>
                        <p class="actualite__resume"><?php the_excerpt(); ?></p>
                    </div>
                    <a class="actualite__btn bouton" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">+ d'infos</a>
                </article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</section>
