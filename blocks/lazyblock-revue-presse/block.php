<section class="container section__presses">
    <div class="presses__content">
        <?php
        $args = array(
            'post_type' => 'revue-presse',
            'posts_per_page' => 10
        );
        $the_query = new WP_Query($args); ?>

        <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <article class="presse__article">
                    <img alt="" class="presse__img" src="<?php the_post_thumbnail(); ?>" >
                    <h3 class="presse__title"><?php the_title(); ?></h3>
                    <a class="presse__btn bouton"  href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        + d'infos
                    </a>
                </article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</section>