<section class="home-presentation">
    <div class="home-presentation__slider-container">
        <button class="w3-button w3-black w3-display-left home-presentation__slider-bouton home-presentation__slider-bouton--gauche" onclick="plusDivs(-1)"><</button>
        <div class="w3-content w3-display-container home-presentation__slider">
            <?php foreach( $attributes['images-du-diapo'] as $inner ): ?>
                <img class="mySlides home-presentation__slide" src="<?php echo esc_url( $inner['image']['url'] ); ?>" alt="<?php echo $inner['alt']; ?>" style="width:100%">
            <?php endforeach; ?>
        </div>
        <button class="w3-button w3-black w3-display-right home-presentation__slider-bouton home-presentation__slider-bouton--droite" onclick="plusDivs(-1)">></button>
    </div>
    <figcaption class="home-presentation__contenu">
        <h1 class="home-presentation__titre"><?php echo $attributes['titre']; ?></h1>
        <h2 class="home-presentation__texte">
            <?php echo $attributes['sous-titre']; ?>        
        </h2>
        <a href="<?php echo esc_url( $attributes['lien-du-bouton'] ); ?>" class="home-presentation__bouton bouton"><?php echo $attributes['texte-du-bouton']; ?></a>
    </figcaption> 
</section>

<section class="home-presentation home-presentation--droite container">
    <figure class="home-presentation__figure">
        <div class="home-presentation__img-container">
            <img src="<?php echo esc_url( $attributes['image-2']['url'] ); ?>" alt="<?php echo $attributes['alt-2']; ?>" class="home-presentation__slide">
        </div>
        <figcaption class="home-presentation__contenu">
            <h2 class="home-presentation__texte">
                <?php echo $attributes['titre-2']; ?>
            </h2>
            <p class="home-presentation__texte-secondaire">
                <?php echo $attributes['sous-titre-2']; ?>
            </p>
            <a href="<?php echo esc_url( $attributes['lien-2'] ); ?>" class="home-presentation__bouton bouton">
                <?php echo $attributes['texte-bouton-2']; ?>
            </a>
        </figcaption> 
    </figure>
</section>