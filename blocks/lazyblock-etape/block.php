<section class="etape">
    <h1 class="etape__titre"><?php echo $attributes['titre']; ?></h1>
    <figure class="etape__intro">
        <?php if ( isset( $attributes['picto-etape']['url'] ) ) : ?>
            <img src="<?php echo esc_url( $attributes['picto-etape']['url'] ); ?>" alt="<?php echo esc_attr( $attributes['picto-etape']['alt'] ); ?>" class="etape__intro-img">
        <?php endif; ?>    
        <figcaption class="etape__intro-contenu">
            <strong class="etape__intro-texte">
                <?php echo $attributes['description']; ?>   
            </strong>
        </figcaption>
    </figure>
    <figure class="etape__presentation">
        <div class="etape__presentation-img-conteneur">
        <?php if ( isset( $attributes['illustration']['url'] ) ) : ?>
            <img src="<?php echo esc_url( $attributes['illustration']['url'] ); ?>" alt="<?php echo esc_attr( $attributes['illustration']['alt'] ); ?>" class="etape__presentation-img">
        <?php endif; ?>    
        </div>
        <figcaption class="etape__presentation-contenu">
            <p class="etape__presentation-texte">
                <?php echo $attributes['contenu-texte']; ?>   
            </p>
        </figcaption>
    </figure>
</section>