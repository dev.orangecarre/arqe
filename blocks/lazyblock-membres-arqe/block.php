<section class="section__industriel container">
    <div class="section__content">
        <div class="section__galleryContainer">
            <h2 class="section__title"><?php echo $attributes['titre-industriel']; ?></h2>
            <div class="section__gallery">
                <?php foreach ($attributes['membres-industriel'] as $inner) : ?>
                    <a href="<?php echo esc_url( $inner['page-industriel'] ); ?>">
                        <img 
                        class="section__image on-hover" 
                        src="<?php echo esc_url( $inner['logo-membre-industriel']['url'] ); ?>" 
                        alt="<?php echo $inner['alt-membre-industriel'] ; ?>"
                        >
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <img class="section__mainImage" src="<?php echo esc_url($attributes['image-industriel']['url']); ?>" alt="<?php echo esc_attr($attributes['image-industriel']['alt']); ?>">
    </div>
</section>
<section class="section__batiment container">
    <div class="section__content">
        <img class="section__mainImage section__mainImage--batiment small" src="<?php echo esc_url($attributes['image-batiment']['url']); ?>" alt="<?php echo esc_attr($attributes['image-batiment']['alt']); ?>">
        <div class="section__galleryContainer">
            <h2 class="section__title section__title--toRight"><?php echo $attributes['titre-batiment']; ?></h2>
            <img class="section__mainImage xl" src="<?php echo esc_url($attributes['image-batiment']['url']); ?>" alt="<?php echo esc_attr($attributes['image-batiment']['alt']); ?>">
            <div class="section__gallery section__gallery--toRight">
            <?php foreach ($attributes['membres-batiment'] as $inner) : ?>
                    <a href="<?php echo esc_url( $inner['page-batiment'] ); ?>">
                        <img 
                        class="section__image on-hover" 
                        src="<?php echo esc_url( $inner['logo-membre-batiment']['url'] ); ?>" 
                        alt="<?php echo $inner['alt-membre-batiment'] ; ?>"
                        >
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>