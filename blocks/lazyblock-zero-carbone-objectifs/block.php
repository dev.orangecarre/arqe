<section class="zero-carbone-description">
    <h2 class="zero-carbone-description__titre"><?php echo $attributes['titre']; ?></h2>
    <article class="zero-carbone-description__desc">
        <h3 class="zero-carbone-description__desc-titre"><?php echo $attributes['description']; ?></h3>
        <div class="zero-carbone-description__labels">
            <?php foreach( $attributes['labels'] as $inner ): ?>
                <div class="zero-carbone-description__label">
                    <img src="<?php echo esc_url( $inner['picto-du-label']['url'] ); ?>" alt="" class="zero-carbone-description__label-img">
                    <h4 class="zero-carbone-description__label-titre"><?php echo $inner['nom-du-label']; ?></h4>
                    <p class="zero-carbone-description__label-desc"><?php echo $inner['description-du-label']; ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    </article>
    <article class="zero-carbone-description__desc">
        <h3 class="zero-carbone-description__desc-titre zero-carbone-description__desc-titre--gauche"><?php echo $attributes['titre-2']; ?></h3>
        <p class="zero-carbone-description__sous-titre"><?php echo $attributes['sous-titre-2']; ?></p>
        <div class="zero-carbone-description__objectifs">
            <?php foreach( $attributes['piliers'] as $inner ): ?>
            <div class="zero-carbone-description__objectif">
                <img src="<?php echo esc_url( $inner['picto-pilier']['url'] ); ?>" alt="" class="zero-carbone-description__objectif-img">
                <p class="zero-carbone-description__objectif-titre"><?php echo $inner['desc-pilier']; ?></p>
            </div>
            <?php endforeach; ?>
        </div>
    </article>
</section>