<section class="section__projets">
    <header class="projets__header">
        <h2 class="projets__title"><?php echo $attributes['titre-section-projet']; ?></h2>
    </header>
    <div class="projets__content">
        <?php
        $args = array(
            'post_type' => 'projet',
            'posts_per_page' => 10
        );
        $the_query = new WP_Query($args); ?>

        <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <article class="projet__article">
                    <img alt="" class="projet__img" src="<?php the_post_thumbnail(); ?>" >
                    <h3 class="projet__title"><?php the_title(); ?></h3>
                    <a class="projet__btn bouton" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >+ d'infos</a>
                </article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</section>