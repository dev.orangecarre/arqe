<section class="slider-second">
    <h2 class="slider-second__titre"></h2>
    <figure class="slider-second__conteneur-bloc">
        <div class="slider-second__image-conteneur">
            <img src="<?php echo get_template_directory_uri() . '/dist/images/logo-ARQE-2.svg'; ?>" alt="logo arqe" class="slider-second__image">
        </div>
        <div class="home-presentation__slider-container home-presentation__slider-container--second">
            <button class="w3-button w3-black w3-display-left home-presentation__slider-bouton home-presentation__slider-bouton--gauche" onclick="plusDivsSecond(-1)"><</button>
            <div class="w3-content w3-display-container home-presentation__slider">
                <?php foreach( $attributes['etapes'] as $inner ): ?>
                    <a href="<?php echo esc_url( $inner['lien-slider'] ); ?>" class="mySlidesSecond">
                        <img class="home-presentation__slide" src="<?php echo esc_url( $inner['image-slider']['url'] ); ?>" alt="<?php echo esc_attr( $inner['alt-slider']['alt'] ); ?>" style="width:100%">
                    </a>
                <?php endforeach; ?>
            </div>
            <button class="w3-button w3-black w3-display-right home-presentation__slider-bouton home-presentation__slider-bouton--droite" onclick="plusDivsSecond(1)">></button>
        </div>
    </figure>
</section>