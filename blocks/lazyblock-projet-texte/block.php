<section class="projet-section">
    <div class="projet-section__img-conteneur">
        <img src="<?php echo esc_url( $attributes['premiere-image']['url'] ); ?>" alt="" class="projet-section__img">
        <img src="<?php echo esc_url( $attributes['deuxieme-image']['url'] ); ?>" alt="" class="projet-section__img"></img>
    </div>
    <div class="projet-section__contenu">
        <p class="projet-section__texte">
            <?php echo $attributes['contenu-texte']; ?>
        </p>
    </div>
</section>