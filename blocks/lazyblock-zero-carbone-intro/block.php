<section class="zero-carbone">
    <h1 class="zero-carbone__titre"><?php echo $attributes['titre']; ?></h1>
    <figure class="zero-carbone__intro">
        <?php if ( isset( $attributes['picto-czc']['url'] ) ) : ?>
            <img src="<?php echo esc_url( $attributes['picto-czc']['url'] ); ?>" alt="<?php echo esc_attr( $attributes['picto-czc']['alt'] ); ?>" class="zero-carbone__intro-img">
        <?php endif; ?>
        <figcaption class="zero-carbone__intro-contenu">
            <strong class="zero-carbone__intro-texte">
                <?php echo $attributes['description']; ?>   
            </strong>
        </figcaption>
    </figure>
    <figure class="zero-carbone__presentation">
        <div class="zero-carbone__presentation-img-conteneur">
            <?php if ( isset( $attributes['illustration']['url'] ) ) : ?>
                <img src="<?php echo esc_url( $attributes['illustration']['url'] ); ?>" alt="<?php echo esc_attr( $attributes['illustration']['alt'] ); ?>" class="zero-carbone__presentation-img">
            <?php endif; ?>
        </div>
        <figcaption class="zero-carbone__presentation-contenu">
            <p class="zero-carbone__presentation-texte">
                <?php echo $attributes['contenu-texte']; ?>   
            </p>
        </figcaption>
    </figure>
</section>