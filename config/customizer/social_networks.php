<?php
/*
 * Theme customizer social network pannel.
 * This file must be included in config/customizer/index.php.
 */

add_action('customize_register', 'neptune_customizer_social_networks');

function neptune_customizer_social_networks($wp_customize)
{
    $wp_customize->add_section('neptune_section_social_networks', [
        'title' => __('Social Networks', 'neptune'),
        'description' => __("Please add your social networks links. Leave empty if you don't want the social network's icon to appear.", 'neptune'),
        'priority' => 81,
    ]);

    // Facebook
    $wp_customize->add_setting('neptune_social_facebook', [
        'transport' => 'refresh',
    ]);

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'neptune_control_social_facebook',
            [
                'label' => __('Facebook Link', 'neptune'),
                //'description' => __('Please enter your Facebook account link', 'neptune'),
                'input_attrs' => [
                    'placeholder' => __('https://', 'neptune'),
                ],
                'section' => 'neptune_section_social_networks',
                'settings' => 'neptune_social_facebook',
                'type' => 'text',
            ]
        )
    );

    // Twitter
    $wp_customize->add_setting('neptune_social_twitter', [
        'transport' => 'refresh',
    ]);

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'neptune_control_social_twitter',
            [
                'label' => __('Twitter Link', 'neptune'),
                //'description' => __('Please enter your Twitter account link', 'neptune'),
                'input_attrs' => [
                    'placeholder' => __('https://', 'neptune'),
                ],
                'section' => 'neptune_section_social_networks',
                'settings' => 'neptune_social_twitter',
                'type' => 'text',
            ]
        )
    );

    // Instagram
    $wp_customize->add_setting('neptune_social_instagram', [
        'transport' => 'refresh',
    ]);

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'neptune_control_social_instagram',
            [
                'label' => __('Instagram Link', 'neptune'),
                //'description' => __('Please enter your instagram account link', 'neptune'),
                'input_attrs' => [
                    'placeholder' => __('https://', 'neptune'),
                ],
                'section' => 'neptune_section_social_networks',
                'settings' => 'neptune_social_instagram',
                'type' => 'text',
            ]
        )
    );

    // LinkedIn
    $wp_customize->add_setting('neptune_social_linkedin', [
        'transport' => 'refresh',
    ]);

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'neptune_control_social_linkedin',
            [
                'label' => __('LinkedIn Link', 'neptune'),
                //'description' => __('Please enter your linkedin account link', 'neptune'),
                'input_attrs' => [
                    'placeholder' => __('https://'),
                ],
                'section' => 'neptune_section_social_networks',
                'settings' => 'neptune_social_linkedin',
                'type' => 'text',
            ]
        )
    );
    
}


add_action('customize_register', 'customizer_footer_logo');

function customizer_footer_logo( $wp_customize ) {

    // Add Settings
    $wp_customize->add_setting('customizer_setting_one', array(
        'transport'         => 'refresh',
        'height'         => 325,
    ));
    $wp_customize->add_setting('customizer_setting_two', array(
        'transport'         => 'refresh',
        'height'         => 325,
    ));
    $wp_customize->add_setting('customizer_setting_three', array(
        'transport'         => 'refresh',
        'height'         => 325,
    ));
    $wp_customize->add_setting('customizer_setting_four', array(
        'transport'         => 'refresh',
        'height'         => 325,
    ));

    // Add Section
    $wp_customize->add_section('slideshow', array(
        'title'             => __('Images "Ils nous font confiance"', 'name-theme'), 
        'priority'          => 70,
    ));    

    // Add Controls
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'customizer_setting_one_control', array(
        'label'             => __('Logo "Ils nous font confiance" #1', 'name-theme'),
        'section'           => 'slideshow',
        'settings'          => 'customizer_setting_one',    
    )));
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'customizer_setting_two_control', array(
        'label'             => __('Logo "Ils nous font confiance" #2', 'name-theme'),
        'section'           => 'slideshow',
        'settings'          => 'customizer_setting_two',
    )));    
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'customizer_setting_three_control', array(
        'label'             => __('Logo "Ils nous font confiance" #3', 'name-theme'),
        'section'           => 'slideshow',
        'settings'          => 'customizer_setting_three',
    )));    
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'customizer_setting_four_control', array(
        'label'             => __('Logo "Ils nous font confiance" #4', 'name-theme'),
        'section'           => 'slideshow',
        'settings'          => 'customizer_setting_four',
    )));    
}



add_action('customize_register', 'customizer_footer_infos');

function customizer_footer_infos( $wp_customize ) {

    // Add Settings
    $wp_customize->add_setting('customizer_setting_infos_address', array(
        'transport'         => 'refresh',
        'height'         => 325,
    ));
    $wp_customize->add_setting('customizer_setting_infos_mail', array(
        'transport'         => 'refresh',
        'height'         => 325,
    )); 

    // Add Section
    $wp_customize->add_section('infos_footer', array(
        'title'             => __('Informations du Pied de page', 'name-theme'), 
        'priority'          => 70,
    ));   

    $wp_customize->add_control(
        new WP_Customize_Control($wp_customize,'customizer_setting_infos_address_control',
            [
                'label' => __('Adresse :', 'nomdusite'),
                'section' => 'infos_footer',
                'settings' => 'customizer_setting_infos_address',
                'type' => 'text',
            ]
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control($wp_customize,'customizer_setting_infos_mail_control',
            [
                'label' => __('Mail :', 'nomdusite'),
                'section' => 'infos_footer',
                'settings' => 'customizer_setting_infos_mail',
                'type' => 'text',
            ]
        )
    );
}