<?php

add_theme_support( 'post-thumbnails', array( 'post' ));

function facyl_post_thumbnails() {
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'facyl_post_thumbnails' );
