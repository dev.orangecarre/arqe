<nav class="nav">
    <div class="container nav__container">

        <a class="nav__lien" href="<?= home_url(); ?>">
            <img class="nav__lien-logo" src="<?php echo get_template_directory_uri() . '/dist/images/logo-arqe.svg'; ?>" alt="Logo Arqe.">
        </a>
        <button class="nav__burger" id="burger">
            <span class="nav__burger-span"></span>
            <span class="nav__burger-span"></span>
            <span class="nav__burger-span"></span>
            </button>
        <?php
        wp_nav_menu(
            array(
                'menu' => 'main_menu',
                'container' => 'div',
                'container_class' => 'menu',
                'items_wrap' => '<ul class="menu__elements">%3$s</ul>'
            )
        );
        ?>

    </div>
</nav>